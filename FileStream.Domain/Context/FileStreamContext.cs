﻿using System.Data.Entity;
using FileStream.Domain.Model.Entity;

namespace FileStream.Domain.Context
{
    public class FileStreamContext : DbContext
    {
        public FileStreamContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
            SetConfigurationOptions();
        }

        public FileStreamContext()
            : this("FS")
        {
        }

        private void SetConfigurationOptions()
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        public virtual DbSet<Document> Document { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.AddFromAssembly(typeof(FileStreamContext).Assembly);
        }
    }
}
