﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Transactions;
using FileStream.Domain.Context;
using FileStream.Domain.Model.Entity;

namespace FileStream.Domain.DAL.Repository
{
    public class DocumentService
    {
        private static FileStreamContext _fileStreamContext;

        public DocumentService()
        {
            _fileStreamContext = new FileStreamContext();
        }

        private const string RowDataStatement =
            @"SELECT Data.PathName() AS 'Path', GET_FILESTREAM_TRANSACTION_CONTEXT() AS 'Transaction' FROM [dbo].[Documents] WHERE Id = @id";

        public IList<Document> GetAll()
        {
            return _fileStreamContext.Document.ToList();
        }

        public Document GetById(Guid id)
        {
            var document = _fileStreamContext.Document.FirstOrDefault(p => p.Id == id);
            if (document == null)
            {
                return null;
            }

            using (var tx = new TransactionScope())
            {
                var selectStatement = String.Format("{0}", RowDataStatement);

                var rowData =
                    _fileStreamContext.Database.SqlQuery<FileStreamRowData>(selectStatement, new SqlParameter("id", id))
                        .First();

                using (var source = new SqlFileStream(rowData.Path, rowData.Transaction, FileAccess.Read))
                {
                    var buffer = new byte[16 * 1024];
                    using (var ms = new MemoryStream())
                    {
                        int bytesRead;
                        while ((bytesRead = source.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            ms.Write(buffer, 0, bytesRead);
                        }
                        document.Data = ms.ToArray();
                    }
                }
                tx.Complete();
            }
            return document;
        }

        public void Update(Document entity)
        {
            using (var tx = new TransactionScope())
            {
                _fileStreamContext.Entry(entity).State = EntityState.Modified;
                _fileStreamContext.SaveChanges();

                SaveDocumentData(entity);

                tx.Complete();
            }
        }

        public void Insert(Document entity)
        {
            using (var tx = new TransactionScope())
            {
                _fileStreamContext.Document.Add(entity);
                _fileStreamContext.SaveChanges();

                SaveDocumentData(entity);

                tx.Complete();
            }
        }

        public void Delete(Guid id)
        {
            var document = _fileStreamContext.Document.FirstOrDefault(p => p.Id == id);
            if (document != null)
            {
                _fileStreamContext.Entry(new Document { Id = id }).State = EntityState.Deleted;
                _fileStreamContext.SaveChanges();
            }
        }

        private static void SaveDocumentData(Document entity)
        {
            var selectStatement = $"{RowDataStatement}";

            var rowData =
                _fileStreamContext.Database.SqlQuery<FileStreamRowData>(selectStatement, new SqlParameter("id", entity.Id))
                    .First();

            using (var destination = new SqlFileStream(rowData.Path, rowData.Transaction, FileAccess.Write))
            {
                var buffer = new byte[16 * 1024];
                using (var ms = new MemoryStream(entity.Data))
                {
                    int bytesRead;
                    while ((bytesRead = ms.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        destination.Write(buffer, 0, bytesRead);
                    }
                }
            }
        }

    }
}
