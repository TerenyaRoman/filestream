﻿namespace FileStream.Domain.DAL.Repository
{
    public class FileStreamRowData
    {
        public string Path { get; set; }
        public byte[] Transaction { get; set; }
    }
}
