namespace FileStream.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Documents",
                c => new
                {
                    // Id = c.Guid(nullable: false, identity: true),
                    UploadTime = c.DateTime(nullable: false),
                    Description = c.String(maxLength: 128),
                    ContentType = c.String(maxLength: 50),
                });
               // .PrimaryKey(t => t.Id);

            Sql("alter table [dbo].[Documents] add [Id] uniqueidentifier rowguidcol not null");
            Sql("alter table [dbo].[Documents] add constraint [UQ_Documents_RowId] UNIQUE NONCLUSTERED ([Id])");
            Sql("alter table [dbo].[Documents] add constraint [DF_Documents_RowId]  default (newid()) for [Id]");
            Sql("alter table [dbo].[Documents] add [Data] varbinary(max) FILESTREAM not null");
            Sql("alter table [dbo].[Documents] add constraint [DF_Documents_Data] default(0x) for [Data]");
        }
        
        public override void Down()
        {
            DropTable("dbo.Documents");

            Sql("alter table [dbo].[Documents] drop constraint [DF_Documents_Data]");
            Sql("alter table [dbo].[Documents] drop column [Data]");
            Sql("alter table [dbo].[Documents] drop constraint [UQ_Documents_Id]");
            Sql("alter table [dbo].[Documents] drop constraint [DF_Documents_Id]");
            Sql("alter table [dbo].[Documents] drop column [Id]");
        }
    }
}
