﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FileStream.Domain.Model.Entity
{
    /// <summary>
    /// Документы(файлы)
    /// </summary>
    public class Document
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        /// <summary>
        /// Дата загрузки
        /// </summary>
        public DateTime UploadTime { get; set; }

        /// <summary>
        /// Описание файла
        /// </summary>
        [MaxLength(128)]
        public string Description { get; set; }

        /// <summary>
        /// Тип файла
        /// </summary>
        [MaxLength(50)]
        public string ContentType { get; set; }

        /// <summary>
        /// Файл(данные)
        /// </summary>
        [NotMapped]
        public byte[] Data { get; set; }
    }
}
