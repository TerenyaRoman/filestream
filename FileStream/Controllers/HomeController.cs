﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using FileStream.Domain.DAL.Repository;
using FileStream.Domain.Model.Entity;
using FileStream.Infrastructure;

namespace FileStream.Controllers
{
    public class HomeController : Controller
    {
        private DocumentService _documentService;
        public HomeController()
        {
            _documentService = new DocumentService();
        }

        /// <summary>
        /// Страница загрузки
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Index(HttpPostedFileBase uploadedFile)
        {
            if (ModelState.IsValid)
            {
                var d = new Document
                {
                    Data = new byte[uploadedFile.ContentLength],
                    UploadTime = DateTime.Now,
                    Description = Path.GetFileNameWithoutExtension(uploadedFile.FileName),
                    ContentType = uploadedFile.ContentType
                };

                await uploadedFile.InputStream.ReadAsync(d.Data, 0, d.Data.Length);
                _documentService.Insert(d);

                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Список файлов
        /// </summary>
        /// <returns></returns>
        public ViewResult Documents()
        {
            var model = _documentService.GetAll()
                .Where(p => p.Description != null)
                .OrderBy(p => p.UploadTime).ToList();

            return View(model);
        }

        /// <summary>
        /// Скачивание файла
        /// </summary>
        /// <param name="fileId">ID файла</param>
        /// <returns></returns>
        public FileContentResult Download(Guid FileId)
        {
            var fileToRetrieve = _documentService.GetById(FileId);
            if (fileToRetrieve != null)
            {
                return File(fileToRetrieve.Data, fileToRetrieve.ContentType, fileToRetrieve.Description + GetDefaultExtension.GetExtension(fileToRetrieve.ContentType));
            }
            return null;
        }

        /// <summary>
        /// Удаление файла
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns></returns>
        public ActionResult Delete(Guid FileId)
        {
            _documentService.Delete(FileId);

            return RedirectToAction("Documents");
        }

        /// <summary>
        /// О программе
        /// </summary>
        /// <returns></returns>
        public ActionResult About()
        {
            return View();
        }

        /// <summary>
        /// Контактная информация
        /// </summary>
        /// <returns></returns>
        public ActionResult Contact()
        {
            return View();
        }
    }
}