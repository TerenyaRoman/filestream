﻿using Microsoft.Win32;

namespace FileStream.Infrastructure
{
    public static class GetDefaultExtension
    {
        public static string GetExtension(string mimeType)
        {
            var key = Registry.ClassesRoot.OpenSubKey(@"MIME\Database\Content Type\" + mimeType, false);
            var value = key != null ? key.GetValue("Extension", null) : null;
            var result = value != null ? value.ToString() : string.Empty;

            return result;
        }
    }
}